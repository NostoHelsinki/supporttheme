<?php get_header(); ?>

	<header class="l-category-header">
	  <div class="row">
	    <div class="medium-12 columns">
	      <div class="m-category-header-icon m-icon-category-manuals-large"></div>
	      <div class="m-category-header">
	      	<?php 
	      		$currentTax = get_query_var('taxonomy');
	      		if( $currentTax ) :
	      			$taxObj = get_taxonomy($currentTax);
    				$post_type = $taxObj->object_type[0]; ?>
			        <h1 class="m-category-header-title"><?php get_post_type_name($post_type); ?></h1>
			        <p class="m-category-header-subtitle"><?php get_post_type_description($post_type); ?></p>
			    <?php endif; ?>
	      </div>
	    </div>
	  </div>
	</header>

	<?php get_template_part('partials/breadcrumb'); ?>

	<section class="l-section-spacing">

	  <!-- Main content -->

	  <div class="row">
	    <div class="medium-12 large-9 columns">

	      <h2 class="m-title"><?php single_cat_title(); ?></h2>

	      <div class="l-category-list">

        	<ul class="m-list">

		      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		      	<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

		  	  <?php endwhile; endif; ?>

	  	  	</ul>

	  	  	<!--<nav class="wp-prev-next">
	            <ul class="clearfix">
	    	        <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "nostotheme")) ?></li>
	    	        <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "nostotheme")) ?></li>
	            </ul>
	        </nav>-->

	    </div>

	  </div>

	  <div class="medium-12 large-3 columns">
		<?php get_sidebar(); ?>
	  </div>

	 </div>

	</section>

<?php get_footer(); ?>
