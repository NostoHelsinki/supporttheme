<?php get_header(); ?>
			
	<!-- Header -->

<header class="l-header-large">
  <div class="row">
    <div class="medium-12 columns">
      <div class="m-header-large">
        <h1 class="m-header-large-title">404</h1>
        <p class="m-header-large-subtitle">Ooops, nothing here.</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="medium-12 large-10 medium-centered columns">
      <h2 class="m-nosto-title">Interested in these?</h2>

      <!-- Nosto -->

      <div class="m-nosto">
      	<?php $post_type_arr = array(
		      						0 => array(
		      							'name' => 'academy',
		      							'description' => 'Quick tips on how to apply use case examples'
		      							),
		      						1 => array(
		      							'name' => 'implementation',
		      							'description' => 'If you have one product that receives a single ...'
		      							),
		      						2 => array(
		      							'name' => 'faq',
		      							'description' => 'Quick tips on how to apply use case examples'
		      							),
		      						3 => array(
		      							'name' => 'manuals',
		      							'description' => 'Quick tips on how to apply use case examples'
		      							)
      							);

				foreach ($post_type_arr as $post_type_el) : 
					$post_type_obj = get_post_type_object($post_type_el["name"]); ?>
					<a href="#" class="m-nosto-single">
			          <div class="m-nosto-single-content">
			            <div class="m-nosto-single-content-image">
			              <img src="<?php echo get_template_directory_uri(); ?>/library/images/icon-<?php echo $post_type_el["name"]; ?>.png" alt="">
			            </div>
			            <div class="m-nosto-single-content-title">
			              <div class="m-nosto-single-content-title-category">
			                <?php echo $post_type_obj->labels->name; ?>
			              </div>
			              <div class="m-nosto-single-content-title-description">
			                <?php echo $post_type_el["description"]; ?>
			              </div>
			            </div>
			          </div>
			        </a>
			    <?php endforeach; ?>

      </div>
    </div>
  </div>

</header>

<?php get_footer(); ?>