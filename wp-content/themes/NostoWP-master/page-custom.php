<?php
/*
Template Name: Custom Page Example
*/
?>

<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
		<header class="l-category-header">
		  <div class="row">
		    <div class="medium-12 columns">
		      <div class="m-category-header-icon m-icon-category-manuals-large"></div>
		      <div class="m-category-header">
		        <h1 class="m-category-header-title"><?php the_title(); ?></h1>
		        <p class="m-category-header-subtitle"><?php get_post_type_description($post->post_name); ?></p>
		      </div>
		    </div>
		  </div>
		</header>

		<?php get_template_part('partials/breadcrumb'); ?>
						
	    <section class="l-section-spacing">
	    	<div class="row">
    			<div class="medium-12 large-9 columns">
    				<h2 class="m-title m-icon-title m-icon-title-category">Categories</h2>
    				<div class="l-category-list">
    						<?php
    							$slug = $post->post_name;
    							$tax = $slug.'_categories';
    							$tax_terms = get_terms($tax);
    							if ($tax_terms) :
    								foreach ($tax_terms as $tax_term) : ?>
    									<div class="m-list-category">
	    									<h3><a class="m-list-category-title" href="<?php echo get_term_link($tax_term); ?>"><?php echo $tax_term->name; ?></a></h3> 								
		    								<?php 
		    									$args = array(
			    									'post_type' => $slug,
			    									'tax_query' => array(
																		array(
																			'taxonomy' => $tax,
																			'field'    => 'slug',
																			'terms'    => $tax_term->slug,
																		),
																	),
			    									'post_status' => 'publish',
			    									'posts_per_page' => '100'
			    								);
		    									$query = new WP_Query($args);
		    									if( $query->have_posts() ) : ?>
		    										<ul class="m-list">
		    											<?php while( $query->have_posts() ) : $query->the_post(); ?>
		    												 <li><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></li>
		    											<?php endwhile; ?>
		    											<?php wp_reset_query(); ?>
		    										</ul>
		    									<?php endif; ?>

	    								</div>
    								<?php endforeach; ?>
    							<?php endif; ?>					        
    				</div>
		    	</div>

		    	<div class="medium-12 large-3 columns">
		    		<?php get_sidebar(); ?>
		    	</div>
		    </div>
		</section> <!-- end .l-section-spacing -->			
		
	<?php endwhile; else : ?>
						
		<?php get_template_part( 'partials/content', 'missing' ); ?>

	<?php endif; ?>
			
<?php get_footer(); ?>
