<?php
/*
Template Name: Home page template
*/
?>

<?php get_header(); ?>
	
	<header class="m-header">
	  <div class="row">
	    <div class="medium-10 large-8 medium-centered columns">
	      <h1 class="m-header-title">How can we help?</h1>
	      <div class="row">
	        <div class="large-12 columns">
	          <?php get_template_part('partials/content', 'search'); ?>
	        </div>
	      </div>
	    </div>
	  </div>
	</header>

	<section class="l-section-spacing">
		<div class="row">

			<div class="medium-12 large-8 columns">

				<h2 class="m-title m-icon-title m-icon-title-category">Support center categories</h2>
				<div class="l-section-category">
					<div class="row">
						<?php $post_type_arr = array('academy', 'implementation', 'faq', 'manuals');
							foreach ($post_type_arr as $post_type_el) : ?>
								<div class="medium-6 columns">
									<?php $post_type_obj = get_post_type_object($post_type_el); ?>
									<a href="<?php echo get_permalink().$post_type_obj->rewrite['slug']; ?>" class="m-category">
										<div class="m-category-icon m-icon-category-<?php echo $post_type_el; ?>"></div>
										<div class="m-category-content">
							                <h3 class="m-category-content-title"><?php echo $post_type_obj->labels->name; ?></h3>
							                <p class="m-category-content-decription"><?php echo $post_type_obj->description; ?></p>
							             </div>
							         </a>
								</div>
							<?php endforeach; ?>
					</div>
				</div>
			</div>

			<div class="medium-12 large-4 columns">

				<?php get_sidebar(); ?>

			</div>

		</div>
	</section>

<?php get_footer(); ?>