<?php get_header(); ?>

	<header class="l-category-header">
	    <div class="row">
	      <div class="medium-12 columns">
	        <div class="m-category-header-icon m-icon-category-manuals-large"></div>
	        <div class="m-category-header">
	        	<?php $post_type = get_post_type( get_the_ID() ); ?>
	          <h1 class="m-category-header-title"><?php get_post_type_name($post_type); ?></h1>
	          <p class="m-category-header-subtitle"><?php get_post_type_description($post_type); ?></p>
	        </div>
	      </div>
	    </div>
	</header>

	<?php get_template_part( 'partials/breadcrumb'); ?>

	<section class="l-section-spacing m-article clearfix">

		<div class="row">

			<div class="medium-12 large-9 columns">	

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

					    

					    	<!-- Main content -->
				        	<h2 class="m-title"><?php the_title(); ?></h2>
				        	<?php get_template_part( 'partials/content', 'byline' ); ?>
							<?php if(has_post_thumbnail()) { the_post_thumbnail('fullwidth'); } ?>
							<?php the_content(); ?>
							<div class="m-article-vote">
								<h4 class="m-article-vote-title">Was this article useful?</h4>
								<?php echo pavo_votes(); ?>
							</div>		
																		
					</article> <!-- end article -->
			    					
				<?php endwhile; ?>

	    			<div class="m-article-nav">

				    	<?php if (function_exists('nosto_infinite_previous_post')) : ?>
				    			<?php nosto_infinite_previous_post('<h6 class="m-article-nav-link-title">&#8592; Previous</h6>', $post_type); ?>
				   	 	<?php endif; ?>

				   	 	<?php if (function_exists('nosto_infinite_next_post')) : ?>
				    			<?php nosto_infinite_next_post('<h6 class="m-article-nav-link-title">Next &#8594;</h6>', $post_type); ?>
				   	 	<?php endif; ?>

			   	 	</div>

			   	<?php else : ?>

			   		<?php get_template_part( 'partials/content', 'missing' ); ?>

			    <?php endif; ?>

		    </div>

			<div class="medium-12 large-3 columns">
				<?php get_sidebar(); ?>
			</div>

	  	</div>

	</section> <!-- end .l-section-spacing m-articlen -->

<?php get_footer(); ?>