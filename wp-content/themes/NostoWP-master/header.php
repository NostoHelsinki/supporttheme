<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- Icons & Favicons -->
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<link href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png" rel="apple-touch-icon" />
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<meta content="website" property="og:type" />
	    <meta content="fi_FI" property="og:locale" />
	    <meta content="Site name" property="og:site_name" />
	    <meta content="Site url" property="og:url" />
	    <meta content="Site title" property="og:title" />
	    <meta content="Site description" />
	    <meta content="Absoulet path to Open Graph image" property="og:image" />

	    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7838432/687846/css/fonts.css" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>

		<!-- Drop Google Analytics here -->
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?>>

		<?php get_template_part( 'partials/nav', 'main-topbar' ); ?>
