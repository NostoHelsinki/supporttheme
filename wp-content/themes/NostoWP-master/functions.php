<?php
ob_start();
/*********************
INCLUDE NEEDED FILES
*********************/

// LOAD CORE FUNCTIONS, REMOVE THIS THE THEME WILL BREAK
require_once(get_template_directory().'/library/nosto.php'); 

// USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
require_once(get_template_directory().'/library/custom-post-type.php');

/*********************
MENUS & NAVIGATION
*********************/
// REGISTER MENUS
register_nav_menus(
	array(
		'main-nav' => __( 'The Main Menu' ),   // main nav in header
		'footer-links' => __( 'Footer Links' ) // nav in footer
	)
);


// THE MAIN MENU
function nosto_main_nav() {
    wp_nav_menu(array(
    	'container' => false,                           // remove nav container
    	'container_class' => '',           // class of container (should you choose to use it)
    	'menu' => __( 'The Main Menu', 'nostotheme' ),  // nav name
    	'menu_class' => 'right',         // adding custom nav class
    	'theme_location' => 'main-nav',                 // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
    	'fallback_cb' => 'nosto_main_nav_fallback'      // fallback function
	));
} /* end nosto main nav */

// THE FOOTER MENU
function nosto_footer_links() {
    wp_nav_menu(array(
    	'container' => '',                              // remove nav container
    	'container_class' => 'footer-links clearfix',   // class of container (should you choose to use it)
    	'menu' => __( 'Footer Links', 'nostotheme' ),   // nav name
    	'menu_class' => 'sub-nav',      // adding custom nav class
    	'theme_location' => 'footer-links',             // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
        'depth' => 0,                                   // limit the depth of the nav
    	'fallback_cb' => 'nosto_footer_links_fallback'  // fallback function
	));
} /* end nosto footer link */

// HEADER FALLBACK MENU
function nosto_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => '',      // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // before each link
        'link_after' => ''                             // after each link
	) );
}

// FOOTER FALLBACK MENU
function nosto_footer_links_fallback() {
	/* you can put a default here if you like */
}

/*********************
SIDEBARS
*********************/

// SIDEBARS AND WIDGETIZED AREAS
function nosto_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar',
		'name' => __('Sidebar', 'nostotheme'),
		'description' => __('The primary sidebar.', 'nostotheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
}

/*********************
ADDING WPML INTO MENU
*********************/
function nosto_new_nav_menu_items ($items, $args) {
    if ($args->theme_location == 'main-nav') {
        $langLink = nosto_custom_menu_flag();
        $loginLink = '<li class="menu-item login-item has-form has-button"><a href="#" class="button">Login</a></li>';
        $items = $items.$langLink;
        $items = $items.$loginLink;   
    }
    return $items; 
}
add_filter( 'wp_list_pages', 'nosto_new_nav_menu_items' );
add_filter( 'wp_nav_menu_items', 'nosto_new_nav_menu_items', 10, 2 );

function nosto_custom_menu_flag () {
    $dropdown = '';
    $active = '';
    $mobile_active = '';
    $mobile_select = '';

    $languages = icl_get_languages();

    if( count($languages) >= 1 ) {
        foreach ($languages as $language) {
            if ($language['active']) {
                $active .= '<a class="language-selector" data-dropdown="language-dropdown" aria-controls="language-dropdown" aria-expanded="false">';
                $active .= '<img src="'.$language['country_flag_url'].'" height="12" alt="'.$language['language_code'].'" width="18" />&nbsp;&nbsp;'.$language['native_name'].'</a>';
                $mobile_active .= '<option selected value="'.$language['url'].'">'.$language['native_name'].'</option>';
            } else {
                $dropdown .= '<li><a class="language-option" href="'.$language['url'].'">';
                $dropdown .= '<img src="'.$language['country_flag_url'].'" height="12" alt="'.$language['language_code'].'" width="18" />&nbsp;&nbsp;'.$language['native_name'].'</a></li>';
                $mobile_select .= '<option value="'.$language['url'].'">'.$language['native_name'].'</option>';
            }
        }
    }
    echo '<ul id="menu-action-navigation" class="nav-lang show-for-large">'.$active.
    '<ul class="nav-language f-dropdown" id="language-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">'.$dropdown.
    '</ul></ul>';
    echo '<div id="mobile-language-select" class="show-for-small"><select onchange="if(this.value) window.location.href=this.value">'.$mobile_active.$mobile_select.'</select></div>';
}
?>