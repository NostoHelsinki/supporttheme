
		<footer class="l-section-spacing">

		  <ul class="m-footer-list">
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">Features</a></li>
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">Pricing</a></li>
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">Terms of Use</a></li>
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">Press</a></li>
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">Jobs</a></li>
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">Contact</a></li>
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">Support</a></li>
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">FAQ &amp; Forums</a></li>
		    <li class="m-footer-list-item"><a class="m-footer-list-item-link" href="#">Login</a></li>
		  </ul>
		  <?php nosto_footer_links(); ?>
		  <div class="text-center">
		    <a href="/" class="m-logo-nosto-inverse"></a>
		  </div>

		</footer>

		<!-- Live Reload script -->
		<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

		<?php wp_footer(); ?>

</body>
</html>