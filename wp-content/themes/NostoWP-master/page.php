<?php get_header(); ?>
						
	<header class="l-category-header">
	  <div class="row">
	    <div class="medium-12 columns">
	      <div class="m-category-header-icon m-icon-category-manuals-large"></div>
	      <div class="m-category-header">
	        <h1 class="m-category-header-title"><?php the_title(); ?></h1>
	        <p class="m-category-header-subtitle"></p>
	      </div>
	    </div>
	  </div>
	</header>

	<?php get_template_part('partials/breadcrumb'); ?>
					
    <section class="l-section-spacing">
    	<div class="row">
			<div class="medium-12 large-9 columns">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    				<h2 class="m-title"><?php the_title(); ?></h2>
    				<?php the_post_thumbnail('full'); ?>
    				<?php the_content(); ?>
    				
    			<?php endwhile; else : ?>
				
					<?php get_template_part( 'partials/content', 'missing' ); ?>

				<?php endif; ?>
	    	</div>

	    	<div class="medium-12 large-3 columns">
	    		<?php get_sidebar(); ?>
	    	</div>
	    </div>
	</section> <!-- end .l-section-spacing -->

<?php get_footer(); ?>