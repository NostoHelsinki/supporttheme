<form action="<?php bloginfo('siteurl'); ?>" method="get" id="searchform" role="search">
    <div class="row collapse">
      <div class="small-12 medium-9 columns">          	
        <input type="text" class="m-search-input" name="s" id="search" placeholder="e.g. How do I setup triggered emails">
        <span class="m-search-input-icon"></span>
      </div>
      <div class="small-12 medium-3 columns">
        <input type="submit" class="button postfix m-search-button" value="Show answers">
      </div>
    </div>
 </form>