<nav class="top-bar" data-topbar role="navigation">

  <ul class="title-area">
    <li class="name">
    	<a class="m-topbar-logo" href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url(get_theme_mod('nosto_logo')); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
        <h1 class="m-topbar-title"><a href="<?php echo home_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><b>Support</b> <span>Center</span></a></h1>
    </li>
    <li class="toggle-topbar menu-icon">
		<a href="#"><span>Menu</span></a>
	</li>
  </ul>

  <section class="top-bar-section">
    <?php nosto_main_nav(); ?>
  </section>

</nav>