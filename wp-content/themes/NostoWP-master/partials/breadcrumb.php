<section id="post-<?php the_ID(); ?>" class="m-breadcrumb">
  <div class="row">
    <div class="medium-12 columns">
      <ul class="m-breadcrumb-location">
        <?php if(function_exists('bcn_display')) { bcn_display(); } ?>
      </ul>
      <form action="<?php bloginfo('siteurl'); ?>" method="get" role="search" class="m-breadcrumb-search">
        <input type="text" name="s" class="m-breadcrumb-search-input" placeholder="Search...">
        <input type="submit" class="m-breadcrumb-search-icon" value="">
      </form>
    </div>
  </div>
</section>
