<?php get_header(); ?>

	<!-- Header -->

	<header class="l-category-header">
	  <div class="row">
	    <div class="medium-12 columns">
	      <div class="m-category-header-icon m-icon-search-large"></div>
	      <div class="m-category-header">
	        <h1 class="m-category-header-title">Search</h1>
	        <p class="m-category-header-subtitle">Find all the gory details about Nosto.</p>
	      </div>
	    </div>
	  </div>
	</header>

	<?php get_template_part('partials/breadcrumb'); ?>

	<section class="l-section-spacing">

	  <!-- Main content -->

	  <div class="row">
	    <div class="medium-12 large-9 columns">

	    	<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
				  's' => $s,
				  'posts_per_page' => 4,
				  'paged' => $paged
				);

				//query_posts($args);
				relevanssi_do_query($args);
			?>
	    	<h2 class="m-title m-title-search"><?php _e('Results', 'nostotheme'); ?> <small><?php echo $num = ($wp_query->found_posts); ?> found</small></h2>
	    	
	    	<?php get_template_part('partials/content', 'search'); ?>

	    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						<header class="article-header">
							<h3 class="m-search-result-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
							<h4 class="m-search-result-category">in <?php echo get_the_category( get_the_ID() ); ?></h4>
							<div class="m-search-result-date"><?php the_time('d.m.Y'); ?></div>		
						</header>		
						<section class="entry-content">
							<?php the_excerpt('<button class="tiny">Read more...</button>'); ?> 
						</section>
					</article>
			
				<?php endwhile; ?>			
				        <?php if (function_exists('nosto_page_navi')) { ?>
					        <?php nosto_page_navi(); ?>
					    <?php } else { ?>
					        <nav class="wp-prev-next">
					            <ul class="clearfix">
					    	        <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "jointstheme")) ?></li>
					    	        <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "jointstheme")) ?></li>
					            </ul>
					        </nav>
					    <?php } ?>
			    <?php else : ?>
			
				    <article id="post-not-found" class="hentry clearfix">
				    	<header class="article-header">
				    		<h1>Sorry, No Results.</h1>
				    	</header>
				    	<section class="entry-content">
				    		<p>Try your search again.</p>
				    	</section>
				    </article>
			    <?php endif; ?>
			    <?php wp_reset_query(); ?>
	    </div>
	  </div>

	</section>

<?php get_footer(); ?>
