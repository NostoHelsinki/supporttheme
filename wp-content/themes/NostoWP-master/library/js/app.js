// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
//$(document).foundation();
jQuery(document).ready(function($) {
	$('.language-selector').click(function () {
		$(this).toggleClass('open');
		$(this).siblings('.f-dropdown').toggleClass('open');
	});

	$(document).foundation();
});