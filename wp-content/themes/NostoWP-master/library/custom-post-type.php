<?php

/* Custom Post Type Example
*/


function custom_post_type() { 

	/* Custom Academy */
	register_post_type( 'academy',
		array('labels' => array(
			'name' => __('Academy', 'nostotheme'),
			'singular_name' => __('Academy', 'nostotheme'),
			'all_items' => __('All Academy', 'nostotheme'),
			'add_new' => __('Add New', 'nostotheme'),
			'add_new_item' => __('Add New Academy', 'nostotheme'),
			'edit' => __( 'Edit', 'nostotheme' ),
			'edit_item' => __('Edit Academy', 'nostotheme'),
			'new_item' => __('New Academy', 'nostotheme'),
			'view_item' => __('View Academy', 'nostotheme'),
			'search_items' => __('Search Academy', 'nostotheme'),
			'not_found' =>  __('Nothing found in the Database.', 'nostotheme'),
			'not_found_in_trash' => __('Nothing found in Trash', 'nostotheme'),
			'parent_item_colon' => ''
			),
			'description' => __( 'Learn how to get most of Nosto by studying guides and best practices.', 'nostotheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'rewrite'	=> array( 'slug' => 'academy', 'with_front' => false ),
			'has_archive' => 'academy',
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	)
	);

	/* Custom Implementation */
	register_post_type( 'implementation',
		array('labels' => array(
			'name' => __('Implementation', 'nostotheme'),
			'singular_name' => __('Implementation', 'nostotheme'),
			'all_items' => __('All Implementation', 'nostotheme'),
			'add_new' => __('Add New', 'nostotheme'),
			'add_new_item' => __('Add New Implementation', 'nostotheme'),
			'edit' => __( 'Edit', 'nostotheme' ),
			'edit_item' => __('Edit Implementation', 'nostotheme'),
			'new_item' => __('New Implementation', 'nostotheme'),
			'view_item' => __('View Implementation', 'nostotheme'),
			'search_items' => __('Search Implementation', 'nostotheme'),
			'not_found' =>  __('Nothing found in the Database.', 'nostotheme'),
			'not_found_in_trash' => __('Nothing found in Trash', 'nostotheme'),
			'parent_item_colon' => ''
			),
			'description' => __( 'Study our implementation guide and get started with Nosto.', 'nostotheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			//'rewrite'	=> array( 'slug' => 'implementation', 'with_front' => false ),
			//'has_archive' => 'implementation',
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	)
	);

	/* Custom FAQ */
	register_post_type( 'faq',
		array('labels' => array(
			'name' => __('FAQ', 'nostotheme'),
			'singular_name' => __('FAQ', 'nostotheme'),
			'all_items' => __('All FAQ', 'nostotheme'),
			'add_new' => __('Add New', 'nostotheme'),
			'add_new_item' => __('Add New FAQ', 'nostotheme'),
			'edit' => __( 'Edit', 'nostotheme' ),
			'edit_item' => __('Edit FAQ', 'nostotheme'),
			'new_item' => __('New FAQ', 'nostotheme'),
			'view_item' => __('View FAQ', 'nostotheme'),
			'search_items' => __('Search FAQ', 'nostotheme'),
			'not_found' =>  __('Nothing found in the Database.', 'nostotheme'),
			'not_found_in_trash' => __('Nothing found in Trash', 'nostotheme'),
			'parent_item_colon' => ''
			),
			'description' => __( 'Join discussion and get answers to your questions right away.', 'nostotheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			//'rewrite'	=> array( 'slug' => 'faq', 'with_front' => false ),
			//'has_archive' => 'faq',
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	)
	);

    /* Custom Manuals */
    register_post_type( 'manuals',
        array('labels' => array(
            'name' => __('Manuals', 'nostotheme'),
            'singular_name' => __('Manual', 'nostotheme'),
            'all_items' => __('All Manuals', 'nostotheme'),
            'add_new' => __('Add New', 'nostotheme'),
            'add_new_item' => __('Add New Manual', 'nostotheme'),
            'edit' => __( 'Edit', 'nostotheme' ),
            'edit_item' => __('Edit Manual', 'nostotheme'),
            'new_item' => __('New Manual', 'nostotheme'),
            'view_item' => __('View Manual', 'nostotheme'),
            'search_items' => __('Search Manual', 'nostotheme'),
            'not_found' =>  __('Nothing found in the Database.', 'nostotheme'),
            'not_found_in_trash' => __('Nothing found in Trash', 'nostotheme'),
            'parent_item_colon' => ''
            ),
            'description' => __( 'Learn all manuals and learn a lot of cool stuff!', 'nostotheme' ), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 8,
            //'rewrite'   => array( 'slug' => 'manuals', 'with_front' => false ),
            //'has_archive' => 'manuals',
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
        )
    );
} 

	add_action( 'init', 'custom_post_type');
	

	/* Start register custom taxonomied for "Academy*/
    register_taxonomy( 'academy_categories', 
    	array('academy'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Academy Categories', 'nostotheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Academy Category', 'nostotheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Academy Categories', 'nostotheme' ), /* search title for taxomony */
    			'all_items' => __( 'All Academy Categories', 'nostotheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Academy Category', 'nostotheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Academy Category:', 'nostotheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Academy Category', 'nostotheme' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Academy Category', 'nostotheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Academy Category', 'nostotheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Academy Category Name', 'nostotheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'academy_categories' ),
    	)
    );   

    register_taxonomy( 'academy_tags', 
    	array('academy'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
    		'labels' => array(
    			'name' => __( 'Academy Tags', 'nostotheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Academy Tag', 'nostotheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Academy Tags', 'nostotheme' ), /* search title for taxomony */
    			'all_items' => __( 'All Academy Tags', 'nostotheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Academy Tag', 'nostotheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Academy Tag:', 'nostotheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Academy Tag', 'nostotheme' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Academy Tag', 'nostotheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Academy Tag', 'nostotheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Academy Tag Name', 'nostotheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    	)
    );
	/* End custom taxonomied for "Academy"

	/* Start register custom taxonomied for "Implementation*/
    register_taxonomy( 'implementation_categories', 
    	array('implementation'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Implementation Categories', 'nostotheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Implementation Category', 'nostotheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Implementation Categories', 'nostotheme' ), /* search title for taxomony */
    			'all_items' => __( 'All Implementation Categories', 'nostotheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Implementation Category', 'nostotheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Implementation Category:', 'nostotheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Implementation Category', 'nostotheme' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Implementation Category', 'nostotheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Implementation Category', 'nostotheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Implementation Category Name', 'nostotheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'implementation_categories' ),
    	)
    );   

    register_taxonomy( 'implementation_tags', 
    	array('implementation'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
    		'labels' => array(
    			'name' => __( 'Implementation Tags', 'nostotheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Implementation Tag', 'nostotheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Implementation Tags', 'nostotheme' ), /* search title for taxomony */
    			'all_items' => __( 'All Implementation Tags', 'nostotheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Implementation Tag', 'nostotheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Implementation Tag:', 'nostotheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Implementation Tag', 'nostotheme' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Implementation Tag', 'nostotheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Implementation Tag', 'nostotheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Implementation Tag Name', 'nostotheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    	)
    );
	/* End custom taxonomied for "Implementation"*/
	

	/* Start register custom taxonomied for "FAQ*/
    register_taxonomy( 'faq_categories', 
    	array('faq'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'FAQ Categories', 'nostotheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'FAQ Category', 'nostotheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search FAQ Categories', 'nostotheme' ), /* search title for taxomony */
    			'all_items' => __( 'All FAQ Categories', 'nostotheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent FAQ Category', 'nostotheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent FAQ Category:', 'nostotheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit FAQ Category', 'nostotheme' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update FAQ Category', 'nostotheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New FAQ Category', 'nostotheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New FAQ Category Name', 'nostotheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'faq_categories' ),
    	)
    );   

    register_taxonomy( 'faq_tags', 
    	array('faq'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
    		'labels' => array(
    			'name' => __( 'FAQ Tags', 'nostotheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'FAQ Tag', 'nostotheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search FAQ Tags', 'nostotheme' ), /* search title for taxomony */
    			'all_items' => __( 'All FAQ Tags', 'nostotheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent FAQ Tag', 'nostotheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent FAQ Tag:', 'nostotheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit FAQ Tag', 'nostotheme' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update FAQ Tag', 'nostotheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New FAQ Tag', 'nostotheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New FAQ Tag Name', 'nostotheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    	)
    );
	/* End custom taxonomied for "FAQ"*/


    /* Start register custom taxonomied for "Manuals*/
    register_taxonomy( 'manuals_categories', 
        array('manuals'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
        array('hierarchical' => true,     /* if this is true, it acts like categories */             
            'labels' => array(
                'name' => __( 'Manual Categories', 'nostotheme' ), /* name of the custom taxonomy */
                'singular_name' => __( 'Manual Category', 'nostotheme' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Manual Categories', 'nostotheme' ), /* search title for taxomony */
                'all_items' => __( 'All Manual Categories', 'nostotheme' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent Manual Category', 'nostotheme' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent Manual Category:', 'nostotheme' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Manual Category', 'nostotheme' ), /* edit custom taxonomy title */
                'update_item' => __( 'Update Manual Category', 'nostotheme' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Manual Category', 'nostotheme' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Manual Category Name', 'nostotheme' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true, 
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'manuals_categories' ),
        )
    );   

    register_taxonomy( 'manuals_tags', 
        array('manuals'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
        array('hierarchical' => false,    /* if this is false, it acts like tags */                
            'labels' => array(
                'name' => __( 'Manual Tags', 'nostotheme' ), /* name of the custom taxonomy */
                'singular_name' => __( 'Manual Tag', 'nostotheme' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Manual Tags', 'nostotheme' ), /* search title for taxomony */
                'all_items' => __( 'All Manual Tags', 'nostotheme' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent Manual Tag', 'nostotheme' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent Manual Tag:', 'nostotheme' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Manual Tag', 'nostotheme' ), /* edit custom taxonomy title */
                'update_item' => __( 'Update Manual Tag', 'nostotheme' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Manual Tag', 'nostotheme' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Manual Tag Name', 'nostotheme' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
        )
    );
    /* End custom taxonomied for "Manuals"*/
?>