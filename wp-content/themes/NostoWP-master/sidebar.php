<div class="row">
	<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
	  <?php dynamic_sidebar( 'sidebar' ); ?>
	<?php endif; ?>

	<!-- Support -->
	  <div class="medium-12 large-12 columns">
	    <div class="l-section-aside">
	      <h2 class="m-title m-title-secondary m-icon-title m-icon-title-contact">Contact support</h2>
	      <a href="#" class="button success small">Contact support</a>
	    </div>
	  </div>

</div>