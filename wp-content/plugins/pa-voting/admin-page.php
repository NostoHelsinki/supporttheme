<?php

/*-----------------------------------------------------------------------------------*/
/* Register the admin page with the 'admin_menu' */
/*-----------------------------------------------------------------------------------*/

function pavo_admin_menu() {
	$page = add_submenu_page( 'options-general.php', __( 'PA Voting', 'pressapps' ), __( 'PA Voting', 'pressapps' ), 'manage_options', 'voting-options', 'pavo_options', 99 );
}
add_action( 'admin_menu', 'pavo_admin_menu' );


/*-----------------------------------------------------------------------------------*/
/* Load HTML that will create the outter shell of the admin page */
/*-----------------------------------------------------------------------------------*/

function pavo_options() {

	// Check that the user is able to view this page.
	if ( ! current_user_can( 'manage_options' ) )
		wp_die( __( 'You do not have sufficient permissions to access this page.', 'pressapps' ) ); ?>

	<div class="wrap">
		<div id="icon-themes" class="icon32"></div>
		<h2><?php _e( 'PA Voting Settings', 'pressapps' ); ?></h2>

		<form action="options.php" method="post">
			<?php settings_fields( 'voting_setup_options' ); ?>
			<?php do_settings_sections( 'voting_setup_options' ); ?>
			<?php submit_button(); ?>
		</form>
	</div>
<?php }

/*-----------------------------------------------------------------------------------*/
/* Registers all sections and fields with the Settings API */
/*-----------------------------------------------------------------------------------*/

function pavo_init_settings_registration() {
	$option_name = 'pavo_options';

	// Check if settings options exist in the database. If not, add them.
	if ( get_option( 'pavo_options' ) )
		add_option( 'pavo_options' );

	// Define settings sections.
	add_settings_section( 'voting_setup_section', __( 'Setup', 'pressapps' ), 'voting_setup_options', 'voting_setup_options' );

	add_settings_field( 'voting', __( 'Voting', 'pressapps' ), 'pavo_settings_field_select', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'voting',
		'class' 			=> '',
		'value'			=> array(
						//		'0' => __( 'Disabled', 'pressapps' ),
								'1' => __( 'Public Voting', 'pressapps' ),
								'2' => __( 'Logged In Users Only', 'pressapps' ),
								),
		'label'			=> __( 'Post article voting.', 'pressapps' ),
	) );
	add_settings_field( 'position', __( 'Position', 'pressapps' ), 'pavo_settings_field_select', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'position',
		'class' 			=> '',
		'value'			=> array(
								'left' => __( 'Left' , 'pressapps' ),
								'center' => __( 'Center', 'pressapps' ),
								'right' => __( 'Right', 'pressapps' ),
								),
		'label'			=> __( 'Select button position.', 'pressapps' ),
	) );
	add_settings_field( 'font_size', __( 'Font Size', 'pressapps' ), 'pavo_settings_field_select', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'font_size',
		'class' 			=> '',
		'value'			=> array(
								'12px' => __( '12px' , 'pressapps' ),
								'13px' => __( '13px', 'pressapps' ),
								'14px' => __( '14px', 'pressapps' ),
								'15px' => __( '15px', 'pressapps' ),
								'16px' => __( '16px', 'pressapps' ),
								'17px' => __( '17px', 'pressapps' ),
								'18px' => __( '18px', 'pressapps' ),
								),
		'label'			=> __( 'Select button font size.', 'pressapps' ),
	) );
	add_settings_field( 'icon', __( 'Icon', 'pressapps' ), 'pavo_settings_field_select', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'icon',
		'class' 			=> '',
		'value'			=> array(
								'none' => __( 'None' , 'pressapps' ),
								'thumbs' => __( 'Thumb', 'pressapps' ),
								'angle' => __( 'Angle', 'pressapps' ),
								'caret' => __( 'Caret', 'pressapps' ),
								),
		'label'			=> __( 'Select vote icon.', 'pressapps' ),
	) );
	add_settings_field( 'vote_like_link', __( 'Vote Like Text', 'pressapps' ), 'pavo_settings_field_text', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'vote_like_link',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Add vote like text.', 'pressapps' ),
	) );
	add_settings_field( 'voted_like_single', __( 'Voted Like Single Text', 'pressapps' ), 'pavo_settings_field_text', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'voted_like_single',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Add voted like single text.', 'pressapps' ),
	) );
	add_settings_field( 'voted_like_plural', __( 'Voted Like Plural Text', 'pressapps' ), 'pavo_settings_field_text', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'voted_like_plural',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Add voted like plural text.', 'pressapps' ),
	) );
	add_settings_field( 'vote_dislike_link', __( 'Vote Dislike Text', 'pressapps' ), 'pavo_settings_field_text', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'vote_dislike_link',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Add vote dislike text.', 'pressapps' ),
	) );
	add_settings_field( 'voted_dislike_single', __( 'Voted Dislike Single Text', 'pressapps' ), 'pavo_settings_field_text', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'voted_dislike_single',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Add voted dislike single text.', 'pressapps' ),
	) );
	add_settings_field( 'voted_dislike_plural', __( 'Voted Dislike Plural Text', 'pressapps' ), 'pavo_settings_field_text', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'voted_dislike_plural',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Add voted dislike plural text.', 'pressapps' ),
	) );
	add_settings_field( 'likes_color', __( 'Likes Color', 'pressapps' ), 'pavo_settings_field_color', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'likes_color',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Set likes button color.', 'pressapps' ),
	) );
	add_settings_field( 'dislikes_color', __( 'Dislikes Color', 'pressapps' ), 'pavo_settings_field_color', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'dislikes_color',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Set dislikes button color.', 'pressapps' ),
	) );
	add_settings_field( 'custom_css', __( 'Custom CSS', 'pressapps' ), 'pavo_settings_field_textarea', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'custom-css',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Add custom CSS code.', 'pressapps' ),
	) );
	add_settings_field( 'reset_all_votes', __( 'Reset All Votes', 'pressapps' ), 'pavo_settings_field_button_reset', 'voting_setup_options', 'voting_setup_section', array(
		'options-name' => $option_name,
		'id'				=> 'reset-all-votes',
		'class'			=> '',
		'value'			=> '',
		'label'			=> __( 'Reset all votes.', 'pressapps' ),
	) );

	// Register settings with WordPress so we can save to the Database
	register_setting( 'voting_setup_options', 'pavo_options', 'voting_options_sanitize' );
}
add_action( 'admin_init', 'pavo_init_settings_registration' );

/*-----------------------------------------------------------------------------------*/
/* add_settings_section() function for the widget options */
/*-----------------------------------------------------------------------------------*/

function voting_setup_options() {
	//echo '<p>' . __( 'You can add voting posts to your site using [voting] shortcode.', 'pressapps' ) . '.</p>';
}

/*-----------------------------------------------------------------------------------*/
/* he callback function to display textareas */
/*-----------------------------------------------------------------------------------*/

function pavo_settings_field_textarea( $args ) {
	// Set the options-name value to a variable
	$name = $args['options-name'] . '[' . $args['id'] . ']';

	// Get the options from the database
	$options = get_option( $args['options-name'] ); ?>

	<label for="<?php echo $args['id']; ?>"><?php esc_attr_e( $args['label'] ); ?></label><br />
	<textarea name="<?php echo $name; ?>" id="<?php echo $args['id']; ?>" class="large-text<?php if ( ! empty( $args['class'] ) ) echo ' ' . $args['class']; ?>" cols="30" rows="10"><?php esc_attr_e( $options[ $args['id'] ] ); ?></textarea>
<?php }


/*-----------------------------------------------------------------------------------*/
/* The callback function to display checkboxes */
/*-----------------------------------------------------------------------------------*/

function pavo_settings_field_checkbox( $args ) {
	// Set the options-name value to a variable
	$name = $args['options-name'] . '[' . $args['id'] . ']';

	// Get the options from the database
	$options = get_option( $args['options-name'] ); ?>

	<input type="checkbox" name="<?php echo $name; ?>" id="<?php echo $args['id']; ?>" <?php if ( ! empty( $args['class'] ) ) echo 'class="' . $args['class'] . '" '; ?>value="<?php esc_attr_e( $args['value'] ); ?>" <?php if ( isset( $options[ $args['id'] ] ) ) checked( $args['value'], $options[ $args['id'] ], true ); ?> />
	<label for="<?php echo $args['id']; ?>"><?php esc_attr_e( $args['label'] ); ?></label>
<?php }


/*-----------------------------------------------------------------------------------*/
/* The callback function to display selection dropdown */
/*-----------------------------------------------------------------------------------*/

function pavo_settings_field_select( $args ) {
	// Set the options-name value to a variable
	$name = $args['options-name'] . '[' . $args['id'] . ']';

	// Get the options from the database
	$options = get_option( $args['options-name'] ); ?>

	<select name="<?php echo $name; ?>" id="<?php echo $args['id']; ?>" <?php if ( ! empty( $args['class'] ) ) echo 'class="' . $args['class'] . '" '; ?>>
		<?php foreach ( $args['value'] as $key => $value ) : ?>
			<option value="<?php esc_attr_e( $key ); ?>"<?php if ( isset( $options[ $args['id'] ] ) ) selected( $key, $options[ $args['id'] ], true ); ?>><?php esc_attr_e( $value ); ?></option>
		<?php endforeach; ?>
	</select>
	<label for="<?php echo $args['id']; ?>" style="display:block;"><?php esc_attr_e( $args['label'] ); ?></label>
<?php }


/*-----------------------------------------------------------------------------------*/
/* The callback function to display text field */
/*-----------------------------------------------------------------------------------*/

function pavo_settings_field_text( $args ) {

	// Set the options-name value to a variable
	$name = $args['options-name'] . '[' . $args['id'] . ']';

	// Get the options from the database
	$options = get_option( $args['options-name'] ); ?>

	<input name="<?php echo $name; ?>" id="<?php echo $args['id']; ?>" type="text" class="regular-text code<?php if ( ! empty( $args['class'] ) ) echo ' ' . $args['class']; ?>" value="<?php if ( isset ( $options[ $args['id'] ] )) { esc_attr_e( $options[ $args['id'] ] ) ;} else { echo ''; } ?>"></input>

	<label for="<?php echo $args['id']; ?>" style="display:block;"><?php esc_attr_e( $args['label'] ); ?></label>
<?php }


/*-----------------------------------------------------------------------------------*/
/* The callback function to display info */
/*-----------------------------------------------------------------------------------*/

function pavo_settings_field_info( $args ) {
	// Set the options-name value to a variable
	$name = $args['options-name'] . '[' . $args['id'] . ']';

	// Get the options from the database
	$options = get_option( $args['options-name'] ); ?>

	<p><?php esc_attr_e( $args['value'] ); ?></p>

<?php }

/*-----------------------------------------------------------------------------------*/
/* Color picker */
/*-----------------------------------------------------------------------------------*/

function wp_enqueue_voting_color_picker( ) {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker-script', plugins_url('js/admin.js',  __FILE__  ), array( 'wp-color-picker' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'wp_enqueue_voting_color_picker' );

/*-----------------------------------------------------------------------------------*/
/* The callback function to display color picker */
/*-----------------------------------------------------------------------------------*/

function pavo_settings_field_color( $args ) {

	// Set the options-name value to a variable
	$name = $args['options-name'] . '[' . $args['id'] . ']';

	// Get the options from the database
	$options = get_option( $args['options-name'] ); ?>

	<input name="<?php echo $name; ?>" id="<?php echo $args['id']; ?>" class="wp-color-picker-field<?php if ( ! empty( $args['class'] ) ) echo ' ' . $args['class']; ?>" value="<?php if ( isset ( $options[ $args['id'] ] )) { esc_attr_e( $options[ $args['id'] ] ) ;} else { echo ''; } ?>"></input>

	<label for="<?php echo $args['id']; ?>" style="display:block;"><?php esc_attr_e( $args['label'] ); ?></label>
<?php }

/*-----------------------------------------------------------------------------------*/
/* Sanitization function */
/*-----------------------------------------------------------------------------------*/

function voting_options_sanitize( $input ) {

	// Set array for the sanitized options
	$output = array();

	// Loop through each of $input options and sanitize them.
	foreach ( $input as $key => $value ) {
		if ( isset( $input[ $key ] ) )
			$output[ $key ] = strip_tags( stripslashes( $input[ $key ] ) );
	}

	return apply_filters( 'voting_options_sanitize', $output, $input );
}

/*-----------------------------------------------------------------------------------*/
/* Reset all votes */
/*-----------------------------------------------------------------------------------*/

function pavo_settings_field_button_reset( $args ) {

	// Set the options-name value to a variable
	$name = $args['options-name'] . '[' . $args['id'] . ']';

	// Get the options from the database
	$options = get_option( $args['options-name'] );
	
	
	$ajax_nonce = wp_create_nonce( "sobish-reset-votes" );
	?>
	<input type="button" name="<?php echo $name; ?>" id="<?php echo $args['id']; ?>" class="button button-secondary" value="<?php echo __( 'Reset all votes', 'pressapps' ) ?>" />
	<label for="<?php echo $args['id']; ?>" style="display:none;"><?php esc_attr_e( $args['label'] ); ?></label>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$("#<?php echo $args['id'];?>").click(function(){
				if ( confirm("Are you want to reset all votes?") ) {
					var data = {
						action : 'reset-all-votes',
						security : '<?php echo $ajax_nonce; ?>',
					};
					
					$.post(ajaxurl, data, function(response) {
						alert(response);
					});
				}
				
			});
		});
	</script>
<?php }

function reset_all_votes_func(){
	check_ajax_referer( 'sobish-reset-votes', 'security' );
	global $wpdb;
	// reset postmeta fields
	$wpdb->query( $wpdb->prepare( "UPDATE $wpdb->postmeta SET meta_value = %d WHERE meta_key = %s", 0, '_votes_likes' ) );
	$wpdb->query( $wpdb->prepare( "UPDATE $wpdb->postmeta SET meta_value = %d WHERE meta_key = %s", 0, '_votes_dislikes' ) );
	
	// reset usermeta fields
	$wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->usermeta WHERE meta_key = %s", 'vote_count' ) );
	die;
}
add_action('wp_ajax_reset-all-votes', 'reset_all_votes_func');
