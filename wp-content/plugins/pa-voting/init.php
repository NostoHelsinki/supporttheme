<?php
/**
 * Plugin Name: PA Voting
 * Plugin URI: http://pressapps.co/plugins/
 * Description: Post Voting Plugin
 * Author: PressApps Team
 * Version : 1.0.0
 */

/* Return option page data */
$pavo_settings = get_option( 'pavo_options' );

define('PRESSAPPS_VOTING_PLUGIN_DIR',dirname(__FILE__));

class PRESSAPPS_VOTING{
    
    /**
     * Setup the Environment for the Plugin
     */
    function __construct() {

        global $pavo_settings;

        include_once 'function.php';
        include_once 'add_action.php';
        include_once 'add_filter.php';
        if ( is_admin() ) {
            include_once( 'admin-page.php' );
        }
   
        load_plugin_textdomain('pressapps', false, basename(dirname(__FILE__)).'/lang' );
        add_action('init' ,array($this,'init'));
        add_action( 'wp_head', 'print_voting_css' );
        add_action('the_content','pavo_add_to_content');

        // Add the users custom CSS if they wish to add any.
        if ( $pavo_settings['custom-css'] ) {
            add_action( 'wp_head', 'print_voting_custom_css' );
        }
    }
    
    function init(){
        
        wp_register_style('pavo_frontend_syles', plugins_url('/css/default.css', __FILE__));
        wp_register_style('pavo_admin_styles', plugins_url('/css/admin.css', __FILE__));
        wp_register_script('pavo_js', plugins_url('/js/custom.js', __FILE__),array('jquery'));

        if ( !is_admin() ) {
            wp_enqueue_style('pavo_frontend_syles');
            wp_enqueue_script('pavo_js');
        } else {
            wp_enqueue_style('pavo_admin_styles');
        }
    }
}

$pressapps_voting = new PRESSAPPS_VOTING();

