/***************************************************
      Post Voting
***************************************************/

jQuery().ready(function(){
    jQuery('a.pa-like_btn').click(function(evt){
        evt.stopPropagation();
        response_div = jQuery(this).parent().parent();
        jQuery.ajax({
            url         : PAVO.base_url,
            data        : {'pa-vote_like':jQuery(this).attr('post_id')},
            beforeSend  : function(){
                
            },
            success     : function(data){
                response_div.html(data).fadeIn(900);
                console.log(data);
            },
            complete    : function(){
                
            }
        });
    });
    
    jQuery('a.pa-dislike_btn').click(function(){

        response_div = jQuery(this).parent().parent();
        jQuery.ajax({
            url         : PAVO.base_url,
            data        : {'pa-vote_dislike':jQuery(this).attr('post_id')},
            beforeSend  : function(){
                
            },
            success     : function(data){
                response_div.html(data).fadeIn(900);
                console.log(data);
            },
            complete    : function(){
                
            }
        });
    });

});
