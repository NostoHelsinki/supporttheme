<?php

/*-----------------------------------------------------------------------------------*/
/* Options CSS */
/*-----------------------------------------------------------------------------------*/

function print_voting_css() {

    global $pavo_settings;

    echo '<style text="text/css"id="voting-style-css">' . "\n";

    if ( $pavo_settings['likes_color'] ) {
        echo '.pa-votes .pa-likes { border-color: ' . sanitize_text_field( $pavo_settings['likes_color'] ) . "}\n" ;
        echo '.pa-votes .pa-likes, .pa-votes .pa-like_btn, .pa-votes .pa-like_btn:hover { color: ' . sanitize_text_field( $pavo_settings['likes_color'] ) . "}\n" ;
    }
    if ( $pavo_settings['dislikes_color'] ) {
        echo '.pa-votes .pa-dislikes { border-color: ' . sanitize_text_field( $pavo_settings['dislikes_color'] ) . "}\n" ;
        echo '.pa-votes .pa-dislikes, .pa-votes .pa-dislike_btn, .pa-votes .pa-dislike_btn:hover { color: ' . sanitize_text_field( $pavo_settings['dislikes_color'] ) . "}\n" ;
    }
    if ( $pavo_settings['position']) {
        echo '.pa-votes { text-align: ' . sanitize_text_field( $pavo_settings['position'] ) . "}\n" ;
    }
    if ( $pavo_settings['font_size']) {
        echo '.pa-likes, .pa-dislikes, .pa-votes i { font-size: ' . sanitize_text_field( $pavo_settings['font_size'] ) . "}\n" ;
    }
       
    echo "</style>\n";
}

function print_voting_custom_css() {

    global $pavo_settings;
    echo '<style text="text/css" id="voting-custom-css">' . "\n" . sanitize_text_field( $pavo_settings['custom-css'] ) . "\n</style>\n";
}

function pavo_add_to_content($content) {
    if ( is_singular('post') ) {
        global $post;
        $content .= pavo_votes();
    }
    return $content;
}

/*-----------------------------------------------------------------------------------*/
/* Voting */
/*-----------------------------------------------------------------------------------*/

function pavo_votes($is_ajax = FALSE) {

    global $pavo_settings;        
    global $post;
    $votes_like = (int) get_post_meta($post->ID, '_votes_likes', true);
    $votes_dislike = (int) get_post_meta($post->ID, '_votes_dislikes', true);
    
    if ($pavo_settings['vote_like_link']) {
        $vote_like_link = $pavo_settings['vote_like_link'];
    } else {
        $vote_like_link = __("I found this helpful", 'pressapps');
    }
    if ($pavo_settings['vote_dislike_link']) {
        $vote_dislike_link = $pavo_settings['vote_dislike_link'];
    } else {
        $vote_dislike_link = __("I did not find this helpful", 'pressapps');
    }

    if ($pavo_settings['voted_like_single']) {
        $voted_like_single = $pavo_settings['voted_like_single'];
    } else {
        $voted_like_single = __("person found this helpful", 'pressapps');
    }
    if ($pavo_settings['voted_dislike_single']) {
        $voted_dislike_single = $pavo_settings['voted_dislike_single'];
    } else {
        $voted_dislike_single = __("person did not find this helpful", 'pressapps');
    }

    if ($pavo_settings['voted_like_plural']) {
        $voted_like_plural = $pavo_settings['voted_like_plural'];
    } else {
        $voted_like_plural = __("people found this helpful", 'pressapps');
    }
    if ($pavo_settings['voted_dislike_plural']) {
        $voted_dislike_plural = $pavo_settings['voted_dislike_plural'];
    } else {
        $voted_dislike_plural = __("people did not find this helpful", 'pressapps');
    }

    $voted_like = sprintf(_n('%s ' . $voted_like_single, '%s ' . $voted_like_plural, $votes_like, 'pressapps'), $votes_like);
    
    $voted_dislike  = sprintf(_n('%s ' . $voted_dislike_single, '%s ' . $voted_dislike_plural, $votes_dislike, 'pressapps'), $votes_dislike);

    $cookie_vote_count      = '';
    $cookie_vote = '';
    if ($pavo_settings['icon'] == 'thumbs') {
        $like_icon = '<i class="paicon-thumbs-up-alt"></i> ';
        $dislike_icon = '<i class="paicon-thumbs-down-alt"></i> ';
    } elseif ($pavo_settings['icon'] == 'angle') {
        $like_icon = '<i class="paicon-angle-up"></i> ';
        $dislike_icon = '<i class="paicon-angle-down"></i> ';
    } elseif ($pavo_settings['icon'] == 'caret') {
        $like_icon = '<i class="paicon-up-dir"></i> ';
        $dislike_icon = '<i class="paicon-down-dir"></i> ';
    } else {
        $like_icon = '';
        $dislike_icon = '';
    }

    if(isset($_COOKIE['vote_count'])){
        $cookie_vote_count = @unserialize(base64_decode($_COOKIE['vote_count']));
    }
    
    if(!is_array($cookie_vote_count) && isset($cookie_vote_count)){
        $cookie_vote_count = array();
    }

    $html = (($is_ajax)?'':'<div class="pa-votes">');
    $vote = $_COOKIE['vote'];

    $class_like = ($vote == 'like') ? ' success' : ''; 
    $class_dislike = ($vote == 'dislike') ? ' success' : ''; 
                            
    if (is_user_logged_in() || $pavo_settings['voting'] == 1 ) :
        
            if(is_user_logged_in())
                $vote_count = (array) get_user_meta(get_current_user_id(), 'vote_count', true);
            else
                $vote_count = $cookie_vote_count;
            
            if (!in_array( $post->ID, $vote_count )) :

                    $html .= '<div class="m-article-vote-button pa-likes"><a class="pa-like_btn" id="abc" post_id="'  . $post->ID . '">'. $like_icon . $vote_like_link . '</a></div>';
                    $html .= '<div class="m-article-vote-button pa-dislikes"><a class="pa-dislike_btn" post_id="' . $post->ID . '">' . $dislike_icon . $vote_dislike_link . '</a></div>';

            else :
                    // already voted
                    $html .= '<button class="m-article-vote-button pa-likes small'.$class_like.'">'. $like_icon .'Yes</button> ';
                    $html .= '<button class="m-article-vote-button pa-dislikes small'.$class_dislike.'">' . $dislike_icon . 'No</button> ';
            endif;
       
    else :
            // not logged in
            $html .= '<button class="m-article-vote-button pa-likes small'.$class_like.'">'. $like_icon  . 'Yes</button> ';
            $html .= '<button class="m-article-vote-button pa-likes small'.$class_dislike.'">' . $dislike_icon . 'No</button> ';
    endif;
    
    $html .= (($is_ajax)?'':'</div>');
	
	
	if ( $is_ajax ) {
		echo $html;
	} else {
		return $html;
	}
}

function pavo_vote() {
    global $post;
    global $pavo_settings;    

    if (is_user_logged_in()) {
        
        $vote_count = (array) get_user_meta(get_current_user_id(), 'vote_count', true);
        
        if (isset( $_GET['pa-vote_like'] ) && $_GET['pa-vote_like']>0) :
                
                $post_id = (int) $_GET['pa-vote_like'];
                $the_post = get_post($post_id);
                
                if ($the_post && !in_array( $post_id, $vote_count )) :
                        $vote_count[] = $post_id;
                        update_user_meta(get_current_user_id(), 'vote_count', $vote_count);
                        $post_votes = (int) get_post_meta($post_id, '_votes_likes', true);
                        $post_votes++;
                        update_post_meta($post_id, '_votes_likes', $post_votes);
                        $post = get_post($post_id);
                        pavo_votes(true);
                        die('');
                endif;
                
        elseif (isset( $_GET['pa-vote_dislike'] ) && $_GET['pa-vote_dislike']>0) :
                
                $post_id = (int) $_GET['pa-vote_dislike'];
                $the_post = get_post($post_id);
                
                if ($the_post && !in_array( $post_id, $vote_count )) :
                        $vote_count[] = $post_id;
                        update_user_meta(get_current_user_id(), 'vote_count', $vote_count);
                        $post_votes = (int) get_post_meta($post_id, '_votes_dislikes', true);
                        $post_votes++;
                        update_post_meta($post_id, '_votes_dislikes', $post_votes);
                        $post = get_post($post_id);
                        pavo_votes(true);
                        die('');
                        
                endif;
                
        endif;

    } elseif (!is_user_logged_in() && $pavo_settings['voting'] == 1) {

        // ADD VOTING FOR NON LOGGED IN USERS USING COOKIE TO STOP REPEAT VOTING ON AN ARTICLE
        $vote_count = '';
        $vote = '';
        
        if(isset($_COOKIE['vote_count'])){
            $vote_count = @unserialize(base64_decode($_COOKIE['vote_count']));
        }
        
        if(!is_array($vote_count) && isset($vote_count)){
            $vote_count = array();
        }
        
        if (isset( $_GET['pa-vote_like'] ) && $_GET['pa-vote_like']>0) :
                
                $post_id = (int) $_GET['pa-vote_like'];
                $the_post = get_post($post_id);
                
                if ($the_post && !in_array( $post_id, $vote_count )) :
                        $vote_count[] = $post_id;
                        $_COOKIE['vote_count']  = base64_encode(serialize($vote_count));
                        $_COOKIE['vote']  = 'like';
                        setcookie('vote_count', $_COOKIE['vote_count'] , time()+(10*365*24*60*60),'/');
                        setcookie('vote', 'like' , time()+(10*365*24*60*60),'/');
                        $post_votes = (int) get_post_meta($post_id, '_votes_likes', true);
                        $post_votes++;
                        update_post_meta($post_id, '_votes_likes', $post_votes);
                        $post = get_post($post_id);
                        pavo_votes(true);
                        die('');
                endif;
                
        elseif (isset( $_GET['pa-vote_dislike'] ) && $_GET['pa-vote_dislike']>0) :
                
                $post_id = (int) $_GET['pa-vote_dislike'];
                $the_post = get_post($post_id);
                
                if ($the_post && !in_array( $post_id, $vote_count )) :
                        $vote_count[] = $post_id;
                        $_COOKIE['vote_count']  = base64_encode(serialize($vote_count));
                        $_COOKIE['vote']  = 'dislike';
                        setcookie('vote_count', $_COOKIE['vote_count'] , time()+(10*365*24*60*60),'/');
                        setcookie('vote','dislike' , time()+(10*365*24*60*60),'/');
                        $post_votes = (int) get_post_meta($post_id, '_votes_dislikes', true);
                        $post_votes++;
                        update_post_meta($post_id, '_votes_dislikes', $post_votes);
                        $post = get_post($post_id);
                        pavo_votes(true);
                        die('');
                        
                endif;
                
        endif;

    } elseif (!is_user_logged_in() && $pavo_settings['voting'] == 2) {

        return;
        
    }
        
}

add_action('init', 'pavo_vote');

add_action('wp_head','pavo_voting_common_js');

function pavo_voting_common_js(){
    $pavo = array(
        'base_url'  => esc_url(home_url()),
    );
    ?>
<script type="text/javascript">
    PAVO = <?php echo json_encode($pavo); ?>;
</script>
    <?php
}




