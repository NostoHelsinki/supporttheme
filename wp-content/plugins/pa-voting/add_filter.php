<?php
/* add vote columns to posts */
function add_votes_columns($columns) {
    return array_merge( $columns, 
              array('likes' => '<a href="' . admin_url('edit.php?orderby=_votes_likes&order=desc') . '">' . __('Likes', 'pressapps') . '</a>', 'dislikes' => '<a href="' . admin_url('edit.php?orderby=_votes_dislikes&order=desc') . '">' . __('Dislikes', 'pressapps') . '</a>' ) );
}
add_filter('manage_posts_columns' , 'add_votes_columns');
