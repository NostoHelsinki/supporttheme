<?php
/**
 * Add the Additional column Values for the Posts
 */

add_action( 'manage_posts_custom_column', 'pavo_manage_post_columns', 10, 2 );

function pavo_manage_post_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'likes' :

            $likes = get_post_meta( $post_id, '_votes_likes', true );
            if ( empty( $likes ) )
                echo '';
            else
                echo $likes;
            break;

        case 'dislikes' :

            $dislikes = get_post_meta( $post_id, '_votes_dislikes', true );
            if ( empty( $dislikes ) )
                echo '';
            else
                echo $dislikes;
            break;

        default :
            break;
    }
}

add_action( 'pre_get_posts', 'pavo_order_by_votes' );

function pavo_order_by_votes( $query ) {
    if( ! is_admin() )
        return;

    $orderby = $query->get( 'orderby');

    if( '_votes_likes' == $orderby ) {
        $query->set('meta_key','_votes_likes');
        $query->set('orderby','meta_value_num');
    }

    if( '_votes_dislikes' == $orderby ) {
        $query->set('meta_key','_votes_dislikes');
        $query->set('orderby','meta_value_num');
    }

}
